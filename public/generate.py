from pathlib import Path
import urllib.parse
import json
import re

def cleanName(name):
    name = name.replace(' - ', '::')
    name = name.replace('_-_', '::')
    name = name.replace('-_-', '::')
    name = name.replace('-', ' ')
    name = name.replace('_', ' ')
    name = name.replace('::', ' - ')
    name = name.replace('None#50', 'None')
    name = name.replace('#u', '')
    name = name.replace('#r', '')
    return name

reversed = True

p = Path("./juki_5/").iterdir()
result = sorted(list(p))
result = result[0:len(result)]

req = {
    "width": 3500,
    "height": 3500
}

req["layerConfigurations"] = []


lc = {
    "editionSize": 100,
}
lc["layers"] = []

for r in result:
    layer = {
        "layerName": re.sub(r'[0-9]+ ', '', r.stem)
    }
    layer["elements"] = []
    p = Path(r).rglob("*.png")
    files = list(p)

    totalWeight = 0
    fileNames = list(map(lambda f: str(f), files))
    commons = []
    for f in files:
        # if "#50" in str(f) or "-50" in str(f):
        if not "#r" in str(f) and not "#u" in str(f):
            commons.append(f)
            continue

        e = {
            "name": cleanName(f.stem),
            "weight": 1
        }

        sameWeightFiles = [1]

        if "#u" in str(f):
            sameWeightFiles = list(filter(lambda f: "#u" in f, fileNames))
            percentage = 35
            e["weight"] = int(100 * (percentage / len(sameWeightFiles)))
            totalWeight = int(totalWeight + e["weight"])
        elif "#r" in str(f):
            sameWeightFiles = list(filter(lambda f: "#r" in f, fileNames))
            percentage = 5
            e["weight"] = int(100 * (percentage / len(sameWeightFiles)))
            totalWeight = int(totalWeight + e["weight"])

        f = urllib.parse.quote(str(f), safe='/')
        e["imageUrl"] = "http://localhost:8080/public/" + f
        layer["elements"].append(e)


    for f in commons:
        e = {
            "name": cleanName(f.stem),
            "weight": 1
        }

        sameWeightFiles = list(filter(lambda f: not "#u" in f and not "#r" in f, fileNames))
        e["weight"] = int((10000 - totalWeight) / len(sameWeightFiles))

        f = urllib.parse.quote(str(f), safe='/')
        e["imageUrl"] = "http://localhost:8080/public/" + f
        # e["imageUrl"] = "raw/" + str(f)
        layer["elements"].append(e)

    if reversed:
        lc["layers"].insert(0, layer)
    else:
        lc["layers"].append(layer)

req["layerConfigurations"].append(lc)

with open("req.json", "w") as outfile:
    json.dump(req, outfile)