const {
    startCreating,
    buildSetup,
    statusReport
} = require('../lib/hashlips/hashlips');

async function status(req, res) {
    res.json(statusReport[req.params.session]);
    return;
}

async function generateImage(req, res) {
    const session = new Date().valueOf();

    buildSetup(session);
    try {
        startCreating(req.body, session);
        res.json({
            status: 'Generating has been started.',
            sessionId: session
        });
        return;
    } catch (e) {
        console.log(e);
        res.status(500).json({
            status: 'error'
        });
    }
}

module.exports = { generateImage, status };