const express = require("express");
const basePath = process.cwd();
const path = require("path");
const { generateImage, status } = require('./controller/generator.js');

function main(){
  const app = express();
  const port = 8080;

  app.use(express.json({ limit: '200mb' }));
  app.use('/public/', express.static(path.join(basePath, 'public')));
  
  app.get("/", (req, res) => res.send("Test"));

  app.post('/generate', generateImage);
  app.get('/status/:session', status);

  app.listen(port, () => {
    console.log(`[server] Server running http://localhost:${port}`);
  });
}

main();